from vpython import *
import math

import plotting as pl
import filemanipulation as fm
import transformation as tr
import classes


op1 = 1
op2 = -1
#Variaveis para as funcoes de transformacao
rotation_axis = ""      #eixo da rotacao
angle = 0               #angulo de rotacao, em graus
distance = [0]*3        #vetor que armazena a distancia da translacao para cada deixo [dx,dy,dz]
reflection_type = ""    #tipo da reflexao: "x","y","z","origem" ou "plano"
plane = [0]*4           #vetor que armazena os coeficientes [a,b,c,d] da equacao do plano ax + by + cz = d
projection_axis = ""    #eixo de projecao, se "x" entao projeta-se no plano x = 0
#variaveis para as funcoes de animacao
fps = 30
animation_duration = 5 #duracao da animacao em segundos
#Arquivos csv da estrutura de dados das imagens
vertex_csv = ""
face_csv = ""

#Gerando a imagem inicial
print("\nCod Operacao")
print("1 Borboleta\n2 Cubo unitario centrado na origem\n3 Cubo unitario com vertice na origem\n0 Finalizar o programa")
while(op2 < 0 or op2 > 3):
    op2 = int(input("\nDigite o codigo: "))
    if(op2 == 1):
        vertex_csv = 'vertices.csv'
        face_csv = 'faces.csv'
    elif(op2 == 2):
        vertex_csv = 'vertices_cubo.csv'
        face_csv = 'faces_cubo.csv'
    elif(op2 == 3):
        vertex_csv = 'vertices_cubo2.csv'
        face_csv = 'faces_cubo2.csv'
    elif(op2 == 0):
        op1 = 0
        op2 = 0
    else:
        print("Codigo invalido")

#Se o programa nao foi finalizado, comecamos a desenhar
if(op1 > 0):
    display = canvas()
    #Gerando os eixos
    axis_size = [5,5,5]
    axis = [None]*3
    pl.draw_axis(axis_size, axis)
    #Gerando a imagem inicial
    image = classes.Image()
    image.vertex = fm.read_vertex_csv(vertex_csv)
    image.face = fm.read_face_csv(face_csv)

    image.vpFace = pl.plot_3Dimage(image)

#Loop de operacao
while( op1 >= 1 and op1 <= 4):
    print("\nCodigo Operacao")
    print("1 Transformar\n2 Animar\n3 Imagem original\n4 Nova imagem\n0 Finalizar o programa")
    op1 = int(input("\nDigite o codigo da operacao: "))
    #========
    #Transformando
    #========
    if(op1 == 1):
        op2 = 1
        while(op2 >= 1 and op2 <= 4):
            print("\nCod Operacao")
            print("1 Rotacao\n2 Translacao\n3 Reflexao\n4 Projecao Isometrica\n0 Sair dessa opcao")
            op2 = int(input("Digite o codigo: "))
            #rotacao
            if(op2 == 1):
                rotation_axis = str(input("Defina o eixo de rotacao (x, y ou z): "))
                angle = float(input("Defina o angulo em graus da rotacao: "))
                angle = (angle*math.pi)/180
                tr.rotation3d(image, angle, rotation_axis)
            #translacao
            elif(op2 == 2):
                distance = input("Insira a distancia ao eixos x, y e z separadas por espaco:").split()
                for i in range(len(distance)):
                    distance[i] = float(distance[i])
                tr.translation3d(image, distance)
            #reflexao
            elif(op2 == 3):
                print("Codigo   tipo\nx     reflexao em torno do eixo x\ny      reflexao em torno do eixo y")
                print("z    reflexao em torno do eixo z\norigem     reflexao em torno da origem\nplano reflexao em torno de plano qualquer")
                reflection_type = input("Digite o codigo")
                if(reflection_type == "plano"):
                    plane = input("Digite os coeficientes da equacao do plano ax + by + cz = d, separados por espaco\n ").split(" ")
                    for i in range(len(plane)):
                        plane[i] = float(plane[i])
                tr.reflection3d(image, reflection_type, plane)
            #Projecao isometrica
            elif(op2 == 4):
                projection_axis = input("Insira o eixo de projecao (x,y ou z) ")
                tr.isometric_projection(image, projection_axis)            
    #========
    #Animando
    #========
    elif(op1 == 2):
        op2 = 1
        while(op2 >= 1 and op2 <= 4):
            print("\nCod Operacao")
            print("1 Animar Rotacao\n2 Animar Translacao\n3 Mudar fps\n4 Mudar duracao da animacao\n0 Sair dessa opcao")
            op2 = int(input("Digite o codigo: "))
            #Animando a rotacao
            if(op2 == 1):
                rotation_axis = str(input("Defina o eixo de rotacao (x, y ou z): "))
                angle = float(input("Defina o angulo em graus da rotacao: "))
                angle = (angle*math.pi)/180
                tr.animate_rotation3d(image, angle, rotation_axis, animation_duration, fps)
            #Animando a translacao
            elif(op2 == 2):
                distance = input("Insira a distancia ao eixos x, y e z separadas por espaco:").split()
                for i in range(len(distance)):
                    distance[i] = float(distance[i])
                tr.animate_translation3d(image, distance, animation_duration, fps)
            #Mudando o fps
            elif(op2 == 3):
                fps = float(input("Defina a nova quantidade de frames por segundo "))
            #Mudando a duracao da animacao
            elif(op2 == 4):
                animation_duration = float(input("Defina a nova duracao da animacao, em segundos "))
    #========
    #Voltando imagem ao estado original
    #========
    elif(op1 == 3):
        image.vertex = fm.read_vertex_csv(vertex_csv)
        image.face = fm.read_face_csv(face_csv)
        tr.update_vertexes(image)
    #========
    #Mudando a imagem
    #========
    elif(op1 == 4):
        print("\nCod Operacao")
        print("1 Borboleta\n2 Cubo unitario centrado na origem\n3 Cubo unitario com vertice na origem")
        op2 = int(input("Digite o codigo: "))
        if(op2 == 1):
            vertex_csv = 'vertices.csv'
            face_csv = 'faces.csv'
        if(op2 == 2):
            vertex_csv = 'vertices_cubo.csv'
            face_csv = 'faces_cubo.csv'
        if(op2 == 3):
            vertex_csv = 'vertices_cubo2.csv'
            face_csv = 'faces_cubo2.csv'
        #Para deleter um objeto do vpython, primeiro eh necessario torna-lo invisivel
        #for i in range(len(image.vpFace)):
        #    image.vpFace[i].visible = False
        #Em seguida deleta-se cada objeto
        #t = len(image.vpFace) -1
        #while (t >= 0):
        #    del(image.vpFace[t])
        #    t -= 1
        pl.delete_VPython_objects(image.vpFace)
        #Criando um novo objeto
        image = classes.Image()
        image.vertex = fm.read_vertex_csv(vertex_csv)
        image.face = fm.read_face_csv(face_csv)
        image.vpFace = pl.plot_3Dimage(image)

pl.delete_VPython_objects(image.vpFace)
pl.delete_VPython_objects(axis)
display.delete()
print("\nPrograma finalizado")