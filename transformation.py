from vpython import *
import numpy as np
import classes
import math
import time

def get_RGB(rgbcode):#e.g. rgbcode = "#FF00FF" saida: R=1,G=0,B=1 ou (1,0,1)
    R = int(rgbcode[1:3],16)/255
    G = int(rgbcode[3:5],16)/255
    B = int(rgbcode[5:],16)/255
    return R,G,B

#==========================
#========FUNCOES 2D========
#==========================

#Funcao para refletir a imagem em qualquer eixo que passe pela origem
def reflection2d(polygon, rx, ry, b): #y = a + bx
    #Reflexao somente em eixos passando pela origem, logo a = 0. Somente nos importamos com o coeficiente angular

    t = pow(-1,ry) #se ry = 1 ha reflexao em y entao t = -1, se ry = 0 nao ha, entao t = 1
    u = pow(-1,rx) #idem para x
    h = 1
    #Primeiro rotaciono a imagem para o novo eixo, se necessario
    if(b != 0): #se b != 0 entao havera reflexao em torno de um eixo qualquer
        rotation2d(polygon, b)

    #matriz de reflexao
    refmatrix = [   [t, 0, 0],
                    [0, u, 0], 
                    [0, 0, h]]
    #Inverto as coordenadas dos vertices
    for i in range(len(polygon.vertex)):#laco para multiplicar cada coordenada
        polygon.vertex[i].coord = np.matmul(refmatrix, polygon.vertex[i].coord)

#------------------

#Funcao para rotacionar a imagem em torno da origem
def rotation2d(polygon, b): #y = a + bx
    ang = math.atan(b) #calculo o angulo 'ang' da reta de tangente b

    t = math.cos(ang)
    u = math.sin(ang)
    h = 1
    #matriz de rotacao
    rotmatrix = [   [t, u, 0],
                    [-u, t, 0],
                    [0, 0, h]]
    
    for i in range(len(polygon.vertex)):#laco para multiplicar cada coordenada
        polygon.vertex[i].coord = np.matmul(rotmatrix, polygon.vertex[i].coord)

#==========================
#========FUNCOES 3D========
#==========================

#Funcao a ser chamada para transformacoes 3D, apos o atributo Image.vertex ter sofrido alteracoes
#Precisamos mudar a posicoes dos triangulos que compoem a imagem em image.vpFace
def update_vertexes(image):
    #image.vpFace = [None] * len(image.face)
    for i in range(len(image.face)):
        color = image.face[i].color
        R,G,B = get_RGB(color)

        v = image.face[i].v1 -1
        vertex1 = vertex( pos=vec( float(image.vertex[v].coord[0]), float(image.vertex[v].coord[1]), float(image.vertex[v].coord[2]) ), color = vector(R,G,B) )
        
        v = image.face[i].v2 -1
        vertex2 = vertex( pos=vec( float(image.vertex[v].coord[0]), float(image.vertex[v].coord[1]), float(image.vertex[v].coord[2]) ), color = vector(R,G,B) )

        v = image.face[i].v3 -1
        vertex3 = vertex( pos=vec( float(image.vertex[v].coord[0]), float(image.vertex[v].coord[1]), float(image.vertex[v].coord[2]) ), color = vector(R,G,B) )

        #surface = triangle( vs = [vertex1, vertex2, vertex3] )
        image.vpFace[i].vs = [vertex1,vertex2,vertex3]
        #image.vpFace[i] = triangle( vs = [vertex1, vertex2, vertex3] )

#------------------
def rotation3d(polygon, rotangle, rotaxis):

    t = math.cos(rotangle)
    u = math.sin(rotangle)
    h = 1
    #Rotacao para o angulo em relacao ao eixo x
    if(rotaxis == "x" or rotaxis == "X"):
        rotMatrix = [    
                    [1, 0, 0, 0],
                    [0, t, -u, 0],
                    [0, u, t, 0],
                    [0, 0, 0, h]
                    ]
    #rotacao para o angulo em relacao ao eixo y
    if(rotaxis == "y" or rotaxis == "Y"):
        rotMatrix = [    
                    [t, 0, u, 0],
                    [0, 1, 0, 0],
                    [-u, 0, t, 0],
                    [0, 0, 0, h]
                    ]
    #rotacao para o angulo em relacao ao eixo z
    if(rotaxis == "z" or rotaxis == "Z"):
        rotMatrix = [    
                    [t, -u, 0, 0],
                    [u, t, 0, 0],
                    [0, 0, 1, 0],
                    [0, 0, 0, h]
                    ]
    
    for i in range(len(polygon.vertex)):
        polygon.vertex[i].coord = np.matmul(rotMatrix, polygon.vertex[i].coord)
    
    update_vertexes(polygon)

#------------------
def animate_rotation3d(polygon, angle, rotaxis, duration, fps):
    totalFrames = fps*duration
    rotationPerFrame = angle/totalFrames
    totalRotation = 0
    while( totalRotation < angle ):

        time.sleep(1/fps)   #Qual o intervalo de tempo entre cada quadro, escolher entre time.sleep() e rate()
        
        rotation3d(polygon, rotationPerFrame, rotaxis)
        totalRotation += rotationPerFrame
        
        #rate(fps)          #Funcao do vpython, quantos frames por segundo. Escolher entre rate() e time.sleep()

#------------------
def translation3d(polygon, distance):
    dx = distance[0]
    dy = distance[1]
    dz = distance[2]
    h = 1
    trMatrix = [
                [1, 0, 0, dx],
                [0, 1, 0, dy],
                [0, 0, 1, dz],
                [0, 0, 0, h]
                ]
    for i in range(len(polygon.vertex)):
        polygon.vertex[i].coord = np.matmul(trMatrix, polygon.vertex[i].coord)
    
    update_vertexes(polygon)

#------------------
def animate_translation3d(polygon, distance, duration, fps):
    totalFrames = fps*duration
    
    distancePerFrame = []   #Vetor que armazena a distancia que cada coordenada andara em cada frame
    absDistancePerFrame = 0 #variavel que calcula a distancia absoluta percorrida em cada frame
    finalDistance = 0       #condicao de parada da animacao, a distancia absoluta total
    totalDistance = 0       #varial que calcula a distancia absoluta percorrida por laco
    #Calculando as informacoes de cada frame e final da animacao
    for i in range(len(distance)):
        distancePerFrame.append( distance[i]/totalFrames )
        finalDistance += abs(distance[i])
        absDistancePerFrame += abs(distancePerFrame[i])
    #Animando    
    while( totalDistance < finalDistance ):
        time.sleep(1/fps)   #Qual o intervalo de tempo entre cada quadro, escolher entre time.sleep() e rate()
        
        translation3d(polygon, distancePerFrame)
        totalDistance += absDistancePerFrame
        
        #rate(fps)          #Funcao do vpython, quantos frames por segundo. Escolher entre rate() e time.sleep()

#------------------
def reflection3d(polygon, ref, plane): #plano qualquer a*X + b*Y + c*Z = d, plane =[a,b,c,d]
    
    #========
    #REFLEXAO EM UM PLANO QUALQUER
    #=======
    #vetor normal do plano
    #[X',Y',Z'] = [x,y,z] + 2*k*Normal, onde Normal = [cx,cy,cz]
    if(ref == "plano"):
        cx = plane[0]
        cy = plane[1]
        cz = plane[2]
        d = plane[3]
        h = 1
        for i in range(len(polygon.vertex)):
            vertex = polygon.vertex[i].coord
            #Primeiro acho o k
            k = (d - cx*vertex[0] - cy*vertex[1] - cz*vertex[2])/(pow(cx,2) + pow(cy,2) + pow(cz,2))
            #Encontro a nova coordenada
            #newVertex = [ vertex[0] + 2*k*cx, vertex[1] + 2*k*cy, vertex[2] + 2*k*cz]
            #polygon.vertex[i].coord = newVertex
            refmatrix = [   [1,      0,      0,      0],
                            [0,      1,      0,      0],
                            [0,      0,      1,      0],
                            [2*k*cx, 2*k*cy, 2*k*cz, h]
                        ]
            polygon.vertex[i].coord = np.matmul(polygon.vertex[i].coord, refmatrix)

    #========
    #REFLEXAO EM TORNO DOS EIXOS
    #========
    if(ref == "x" or ref == "y" or ref == "z" or ref == "origem"):
        #a posicao ref[0] dos diz se ha reflexao em torno do plano x = 0
        #portanto, ref[1,1,1] para reflexao em torno da origem
        #t = pow(-1,ref[0])#Se ref[0] == 1, entao t = -1, portanto, havera reflexao em torno do plano x = 0 
        #u = pow(-1,ref[1])
        #v = pow(-1,ref[2])
        
        t = u = v = 1
        if(ref == "x"):
            t = -1
        elif(ref == "y"):
            u = -1
        elif(ref == "z"):
            v = -1
        elif(ref == "origem"):
            t = u = v = -1

        h = 1

        #matriz de reflexao
        refmatrix = [   [t, 0, 0, 0],
                        [0, u, 0, 0], 
                        [0, 0, v, 0],
                        [0, 0, 0, h]    ]
        #Inverto as coordenadas dos vertices
        for i in range(len(polygon.vertex)):#laco para multiplicar cada coordenada
            polygon.vertex[i].coord = np.matmul(polygon.vertex[i].coord, refmatrix)

    update_vertexes(polygon)

#------------------
#Funcao para projecao paralela isometrica
def isometric_projection(polygon, axis_projection):
    angy = 44
    angx = 35.26

    t = math.cos(angy)
    u = math.sin(angy)
    
    v = math.cos(angx)
    w = math.sin(angx)
    
    h = 1
    
    #Xunit = [v, u*w, 0, 1]
    #Yunit = [0, v, 0, 1]
    #Zunit = [u, -u*w, 0, 1]
    #Para Xunit = Yunit entao u^2 = (v^2)/(1 - v^2)
    #Para Zunit = Yunit entao u^2 = (1 - 2*v^2)/(1 - v^2)
    #igualando temos que angy = 45 e angx = 35.26
    #Reducao F = sqrt(v^2)
    
    #Resultado da multiplicacao da rotacao em y por angy e em x por angx 
    rotmatrix = [   [t, u*w, -u*v, 0],
                    [0, v,  w, 0],
                    [u, -w*t, v*t, 0],
                    [0, 0,  0,  1]    ]

    #Matriz de projecao no plano axisprojection = 0
    x = 1
    y = 1
    z = 1
    h = 1
    if(axis_projection == "x"):
        x = 0
    if(axis_projection == "y"):
        y = 0
    if(axis_projection == "z"):
        z = 0
    projmatrix = [  [x, 0, 0, 0],
                    [0, y, 0, 0],
                    [0, 0, z, 0],
                    [0, 0, 0, h]  ]

    isomatrix = np.matmul(rotmatrix, projmatrix)

    for i in range(len(polygon.vertex)):
        polygon.vertex[i].coord = np.matmul(polygon.vertex[i].coord, isomatrix)
        #polygon.vertex[i].coord = np.matmul(isomatrix, polygon.vertex[i].coord)
    #for i in range(len(polygon.vertex)):
    #    polygon.vertex[i].coord = np.matmul(isomatrix, polygon.vertex[i].coord)

    update_vertexes(polygon)

#------------------

#Funcao para projecao no eixo axis_projection = 0
def projection(polygon, axis_projection):
    x = 1
    y = 1
    z = 1
    h = 1
    if(axis_projection == "x"):
        x = 0
    if(axis_projection == "y"):
        y = 0
    if(axis_projection == "z"):
        z = 0
    projmatrix = [  [x, 0, 0, 0],
                    [0, y, 0, 0],
                    [0, 0, z, 0],
                    [0, 0, 0, h]  ]
                    
    for i in range(len(polygon.vertex)):
        polygon.vertex[i].coord = np.matmul(projmatrix, polygon.vertex[i].coord)

    update_vertexes(polygon)

#------------------

def isometric_rotation(polygon):
    angy = 45
    angx = 35.26

    t = math.cos(angy)
    u = math.sin(angy)
    
    v = math.cos(angx)
    w = math.sin(angx)
    
    h = 1
    
    #Xunit = [v, u*w, 0, 1]
    #Yunit = [0, v, 0, 1]
    #Zunit = [u, -u*w, 0, 1]
    #Para Xunit = Yunit entao u^2 = (v^2)/(1 - v^2)
    #Para Zunit = Yunit entao u^2 = (1 - 2*v^2)/(1 - v^2)
    #igualando temos que angy = 45 e angx = 35.26

    #Resultado da multiplicacao da rotacao em y por angy e em x por angx 
    rotmatrix = [   [t, u*w, -u*v, 0],
                    [0, v,  w, 0],
                    [u, -w*t, v*t, 0],
                    [0, 0,  0,  1]    ] 

    for i in range(len(polygon.vertex)):
        polygon.vertex[i].coord = np.matmul(rotmatrix, polygon.vertex[i].coord)
    
    update_vertexes(polygon)