# cg2020.1
Feito por André Fernandes Gonçalves para a matéria de Computação Gráfica durante o período 2020.1
Para executar, rode o programa main.py
O arquivo transformation.py trata somente de transformações nas figuras, e.g., rotação, projeção, etc.
O arquivo plotting.py possui as funções de gerar as imagens em si, seja em 2d ou 3d.
O arquivo filemanipulation.py é o responsável por ler as informações dos arquivos csv.
O arquivo classes.py simplesmente armazena as declarações das classes usadas no programa.
O arquivo main.py somente está configurada para a imagem 3d. A utilização das funções para 2d está comentada no arquivo plotting.py.
