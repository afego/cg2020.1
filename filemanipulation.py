import classes
import csv

#Arquivo para manipulacao de arquivos para leitura

#Funcao para ler e retornar as coordenadas dos vertices
def read_vertex_csv(filename = 'vertices.csv'):#Nome padrao do arquivo
    vertex = []
    
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        #next(f) #Pulando a primeira linha do arquivo csv, caso seja o titulo das colunas
        i=0
        
        for line in reader:

            vertex.append(classes.Vertex())

            vertex[i].coord[0] = float(line[1]) #x
            vertex[i].coord[1] = float(line[2]) #y
            vertex[i].coord[2] = float(line[3]) #z
            vertex[i].coord[3] = float(line[4]) #homogenea

            i = i + 1

        # for i in range(len(vertex)):
        #     print("("+str(vertex[i].x)+", "+str(vertex[i].y)+")")
            
    return vertex

#---------------------------------------------

#Funcao para ler e retornar os vertices que compoem cada face
def read_face_csv(filename = 'faces.csv'):#Nome padrao do arquivo
    face = []
    
    with open(filename, 'r') as f:
        reader = csv.reader(f)
        #next(f) #Pulando a primeira linha do arquivo csv, caso seja o titulo das colunas
        i=0
        for line in reader:

            face.append(classes.Face())

            face[i].v1 = int(line[1])
            face[i].v2 = int(line[2])
            face[i].v3 = int(line[3])
            face[i].color = line[4]

            i = i + 1

    return face