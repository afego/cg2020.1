import matplotlib.pyplot as plt #Pyplot para geracao de imagem em 2D
from vpython import * #vpython para geracao de imagem em 3D
import numpy as np
import math
import random                   #Usada na tentativa de criar o plano, pode ser retirado
import time                     #Para medir o tempo entre as animacoes, pode ser retirado

import filemanipulation as fm
import transformation as tr
import classes

#=============================
#===========FUNCOES===========
#=============================

#---------------------------------------------

#Funcao para gerar a imagem atraves dos vertices e faces
def plot_2Dimage(image, imagename):

    plt.axes()
    t = 1
    next_color=''
    color= '' #cor da face
    filename= imagename+'_front.png'
    #laco para desenhar a figura de frente
    for i in range(len(image.face)):
        color = image.face[i].color
        
        #Identificando as coordenadas dos vertices que definem a face
        v = image.face[i].v1 -1 #v1[i] armazena o a posicao n-1 do vertice Vn no arquivo vertices (e.g. V1 esta na linha 0)
        vertex1 = [ image.vertex[v].coord[0], image.vertex[v].coord[1] ]

        v = image.face[i].v2 -1
        vertex2 = [ image.vertex[v].coord[0], image.vertex[v].coord[1] ]

        v = image.face[i].v3 -1
        vertex3 = [ image.vertex[v].coord[0], image.vertex[v].coord[1] ]

        coord = [vertex1, vertex2, vertex3]
        #face = plt.Polygon(coord, closed=True, edgecolor='#000000', facecolor=color)
        face = plt.Polygon(coord, closed=True, facecolor=color)

        #Colocando a face no grafico
        plt.gca().add_patch(face)

        #Mudando o nome da imagem gerada
        if(i+1<len(image.face)):
            next_color = image.face[i+1].color
        if(next_color != color):#Se i for igual a metade dos vertices, comecou a desenhar as costas da figura
            filename=imagename+str(t)+'.png'
            t+=1
            #Gerando a figura
            plt.axis('scaled')
            plt.savefig(filename)
            plt.show()
            plt.clf() #Limpando a figura, mas mantendo a janela aberta

#---------------------------------------------

#Funcao para a criacao da imagem em 3d
def plot_3Dimage(image):
    faceslist = []
    for i in range(len(image.face)):
        
        color = image.face[i].color
        R,G,B = tr.get_RGB(color)

        v = image.face[i].v1 -1
        vertex1 = vertex( pos=vec( float(image.vertex[v].coord[0]), float(image.vertex[v].coord[1]), float(image.vertex[v].coord[2]) ), color = vector(R,G,B) )
        
        v = image.face[i].v2 -1
        vertex2 = vertex( pos=vec( float(image.vertex[v].coord[0]), float(image.vertex[v].coord[1]), float(image.vertex[v].coord[2]) ), color = vector(R,G,B) )

        v = image.face[i].v3 -1
        vertex3 = vertex( pos=vec( float(image.vertex[v].coord[0]), float(image.vertex[v].coord[1]), float(image.vertex[v].coord[2]) ), color = vector(R,G,B) )

        face = triangle( vs = [vertex1, vertex2, vertex3] )
        faceslist.append(face)
    return faceslist

#---------------------------------------------
#Funcoes auxilares

#Exclue os objetos do VPython
def delete_VPython_objects(array):
    for i in range(len(array)):
        array[i].visible = False
    t = len(array) -1
    while(t >= 0):
        del(array[t])
        t -=1

#Desenha tres setas que simulam os eixos
def draw_axis(axis_size, axis):
    #Eixo X = vermelho
    ax = arrow( pos=vector(0,0,0), axis=vector(axis_size[0],0,0), shaftwidth=0.1, color=vector(1,0,0) )
    axis[0] = ax
    #Eixo Y = Verde
    ax = arrow( pos=vector(0,0,0), axis=vector(0,axis_size[1],0), shaftwidth=0.1, color=vector(0,1,0) )
    axis[1] = ax
    #Eixo Z = Azul
    ax = arrow( pos=vector(0,0,0), axis=vector(0,0,axis_size[2]), shaftwidth=0.1, color=vector(0,0,1) )
    axis[2] = ax

#Desenha um plano qualquer criando 4 pontos aleatorios pertences ao plano
def draw_plane(plane):
    #plano: ax + by + cz = d
    a = plane[0]
    b = plane[1]
    c = plane[2]
    d = plane[3]
    #vertices = [ [],[],[],[] ]
    vertices = []
    #definindo os 4 vertices do retangulo que simulara o plano
    for i in range(4):
        x = random.randrange(-2,2)
        y = random.randrange(-2,2)
        z = (d -a*x -b*y)/c
        #vertices[i] = [x,y,z]
        vertices.append([x,y,z])
    vertex1 = vertex( pos = vec(vertices[0][0], vertices[0][1], vertices[0][2]), color = vector(1,0,0) )
    vertex2 = vertex( pos = vec(vertices[1][0], vertices[1][1], vertices[1][2]), color = vector(0,1,0) )
    vertex3 = vertex( pos = vec(vertices[2][0], vertices[2][1], vertices[2][2]), color = vector(0,0,1) )
    vertex4 = vertex( pos = vec(vertices[3][0], vertices[3][1], vertices[3][2]), color = vector(1,0,1) )
    quad( vs = [vertex1, vertex2, vertex3, vertex4] )  

#=============================
#======PROGRAMA PRINCIPAL=====
#=============================

#=======
#Criando e desenhando as figuras
#=======

#NOMES DOS ARQUIVOS DE VERTICES E FACES
#vertex_csv = 'vertices.csv'
#face_csv = 'faces.csv'

#Instanciamento das imagens a serem desenhadas
#butterfly = classes.Image()

#Leitura dos vertices e faces
#butterfly.vertex = fm.read_vertex_csv(vertex_csv)
#butterfly.face = fm.read_face_csv(face_csv)

#Outras imagens
#cube_vertex_csv = 'vertices_cubo.csv'
#cube_face_csv = 'faces_cubo.csv'
#cube = classes.Image()
#cube.vertex = fm.read_vertex_csv(cube_vertex_csv)
#cube.face = fm.read_face_csv(cube_face_csv)

#cube2_vertex_csv = 'vertices_cubo2.csv'
#cube2_face_csv = 'faces_cubo2.csv'
#cube2 = classes.Image()
#cube2.vertex = fm.read_vertex_csv(cube2_vertex_csv)
#cube2.face = fm.read_face_csv(cube2_face_csv)

#=================================================================
#==========================IMAGEM 2D==============================
#=================================================================
#Desenhando
#plot_2Dimage(butterfly, "butterfly2d")

#booleano para sinalizar se havera reflexao no eixo x
#reflection_in_x = 0
#booleano para sinalizar se havera reflexao no eixo y
#reflection_in_y = 0

#coeficiente angular da reta que passa pela origem
#b != 0 para reta generica na origem
#b = 1

#tr.reflection2d(butterfly, reflection_in_x, reflection_in_y, b)
#plot_2Dimage(butterfly, "butterfly2d_reflected")

#=================================================================
#==========================IMAGEM 3D==============================
#=================================================================
#tamanho da setas que simulam os eixos
#axis_size = [5,5,5]
#draw_axis(axis_size)

#Desenhando a imagem original
#cube2.vpFace = plot_3Dimage(cube2)
#=======
#Rotacao
#=======

#rotationAngle = 180
#rotationAngle = (rotationAngle * math.pi)/180
#rotationAxis = "y"
#tr.rotation3d(cube2, rotationAngle, rotationAxis)
#---Animando---
#fps = 30
#animation_duration = 3
#begin = time.time()
#tr.animate_rotation3d(cube2,rotationAngle,rotationAxis, animation_duration, fps)
#end = time.time()
#print(end - begin)

#=======
#Translacao
#=======

#axisDistance =[3,0,0]
#tr.translation3d(cube2, axisDistance)
#---Animando---
#fps = 30
#animation_duration = 3
#begin = time.time()
#tr.animate_translation3d(cube2, axisDistance, animation_duration, fps)
#end = time.time()
#print(end - begin)

#=======
#Reflexao
#=======
#Tipo da reflexao: 1 em torno de plano qualquer, 0 para algum dos eixos, inclusive origem
#refType = 1
#variaveis para definir em relacao a quais eixos serao refletidos.eg. ref = [1,1,1] para origem, ref = [0,1,0] para o plano y = 0
#ref = [1,1,1]
#refAxis = "x"
#variaveis para definir os coeficientes da equacao do plano qualquer cx*X + cy*Y +cz*Z = d
#plane = [1,1,1,1] #plano: ax + by + cz = d, onde plane[a,b,c,d]
#Desenhando o plano para facilitar a visualizacao da reflexao
#draw_plane(plane)

#tr.reflection3d(cube2, refType, refAxis, plane)
#tr.reflection3d(cube2, refAxis, plane)


#=======
#Projecao isometrica
#=======

#axis_projection = "y"
#tr.projection(butterfly, axis_projection)
#tr.isometric_rotation(butterfly)
#tr.isometric_projection(butterfly, axis_projection)
